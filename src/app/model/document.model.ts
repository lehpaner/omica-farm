﻿export interface IDocument {
    nomeFile: string;
    alfrescoRefFile: string;
    nomeFileOriginale: string;
    tipoFile: number;
    statoFile: number;
    uploadedOn: Date;
}