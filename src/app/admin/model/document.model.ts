﻿export interface IDocument {
    nome: string;
    path: string;
    descrizione: string;
    editable: boolean;
}