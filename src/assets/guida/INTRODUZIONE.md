# Albo nazionale dei componenti delle commissioni giudicatrici

### Introduzione

L’albo nazionale obbligatorio dei componenti delle commissioni giudicatrici è istituito presso l'Autorità Nazionale Anticorruzione ai sensi dell’art. 78 del D.Lgs. 50/2016. L’Autorità lo gestisce e lo aggiorna secondo criteri individuati nelle linee guida n. 5.
La pagina di atterraggio del servizio fornisce le istruzioni di base per accedere all’albo in qualità di esperto o di interessato.
Gli esperti che intendono candidarsi devono essere in possesso di credenziali username e password rilasciate dall’Autorità, di una casella di posta elettronica certificata (PEC) e di un dispositivo di firma digitale.

![Land](./assets/guida/land.jpg)

Il menu è disponibile sia nell’angolo in alto a sinistra dell’interfaccia 

![Menu](./assets/guida/menu1.jpg)

sia nella pulsantiera in alto a destra

![Toolbar](./assets/guida/toolbar.jpg)

e consente l’accesso alle seguenti sezioni:
* Guida: il presente manuale
* Consultazioni: per la libera consultazione dell’albo (cfr paragrafo Consultazioni)
* Iscrizioni: per chi intende iscriversi oppure è già iscritto e vuole aggiornare i dati della propria iscrizione (cfr paragrafo Iscrizioni). In entrambi i casi l’utente verrà indirizzato alla maschera di login dove dovrà fornire le credenziali username e password precedentemente acquisite all’indirizzo [https://servizi.anticorruzione.it/portal/classic/GestioneUtenti/RegistrazioneUtente](https://servizi.anticorruzione.it/portal/classic/GestioneUtenti/RegistrazioneUtente)

### Accesso 

![login](./assets/guida/login.jpg)

![logged](./assets/guida/menu_utente_logged.jpg)


![logout](./assets/guida/menu_logout.jpg)